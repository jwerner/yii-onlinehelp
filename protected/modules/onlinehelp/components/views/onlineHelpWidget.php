<?php
$yourscript_asset= Yii::app()->assetManager->publish(dirname(__FILE__).'/../../assets/js/');  
  
//Register JS and CSS files          
Yii::app()->clientScript->registerScriptFile($yourscript_asset. '/shortcut.js');  
$parser = new CMarkdownParser;
$checkbox = '<hr/><p>'
    .'<input type="checkbox" name="hidePopupGuiders" id="hidePopupGuiders" value="1" ';
if($showPopupGuiders==true)
    $checkbox .= 'checked="checked" ';
$checkbox .=  'style="display:inline" onclick="togglePageHelp(this, \''.$requestId.'\');" />&nbsp;';
$checkbox .=  CHtml::encode(Yii::t('onlinehelp','Show Popup Help for this page')) . '</p>';

// Do we have a page help item?
if(!is_null($model)) {
    $title = ' :: '.$model->title;
    $label = Yii::t('onlinehelp','Update');
    $url = array('/onlinehelp/editor/update','id'=>$model->id);
    $content = $parser->safeTransform($model->content);
    // {{{ Page Help Guider
    $cfg = array(
        'id'          => 'olHelpGuider_'.$model->id,
        'title'       => $model->title,
        'buttons'     => array(),
        'description' => $content.$checkbox,
        'overlay'     => true,
        'xButton'     => true,
        // look here !! 'show' is true, so that means this guider will be
        // automatically displayed when the page loads
        'show'        => $showPopupGuiders,
        'autoFocus'      => true
    ); 
    if( $userIsEditor==true ) {
        $cfg['description'] .= '<p>'.CHtml::link(
            '<i class="icon-pencil icon-white"></i> '.Yii::t('onlinehelp','Update'), 
            array('/onlinehelp/editor/update', 'id'=>$model->id),
            array('class'=>'btn btn-success')
        ).'</p>';
    }
    if(!is_null($models)) {
        $cfg['next'] = 'olHelpGuider_'.$models[0]->id;
        $cfg['buttons'][] = array(
            'name'   => Yii::t('onlinehelp','Next'),
            'onclick'=> "js:function(){guiders.next();}"
        );
    }
    $cfg['buttons'][] = array(
        'name'   => Yii::t('onlinehelp','Exit'),
        'onclick'=> "js:function(){guiders.hideAll();}"
    );
    $this->widget('ext.eguiders.EGuider', $cfg);
    // }}}  
} else {
    $title = '';
    $label = Yii::t('onlinehelp','Create');
    $url = array('/onlinehelp/editor/create','OnlineHelp[requestId]'=>str_replace('/', '.', $requestId));
    $content = Yii::t('onlinehelp','Sorry, no help text available');
}
if(!is_null($models)) { // {{{ Element-specific help guiders
    foreach($models as $n=>$olHelp) {
        $cfg = array( 
            'id'          => 'olHelpGuider_'.$olHelp->id,
            'title'       => $olHelp->title,
            'description' => $parser->safeTransform($olHelp->content),
            'overlay'     => false,
            'xButton'     => true,
            'attachTo'    => $olHelp->elementId,
            'autoFocus'   => true,
        );
        if(trim($olHelp->guiderPosition)!=='')
            $cfg['position'] = $olHelp->guiderPosition;
    
        if( $userIsEditor==true ) {
            $cfg['description'] .= '<p>'.CHtml::link(
                '<i class="icon-pencil icon-white"></i> '.Yii::t('onlinehelp','Update'), 
                array('/onlinehelp/editor/update', 'id'=>$olHelp->id),
                array('class'=>'btn btn-success')
            ).'</p>';
        }
        $buttons = array();
        if($n==0 and !is_null($model) ) {
            $buttons[] = array(
                'name'   => Yii::t('onlinehelp','Previous'),
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('olHelpGuider_".$model->id."');}"
            );
        }
        if( $n>0 ) {
            $buttons[] = array(
                'name'   => Yii::t('onlinehelp','Previous'),
                'onclick'=> "js:function(){guiders.hideAll(); $('.highlight pre').hide(); guiders.show('olHelpGuider_".$models[$n-1]->id."');}"
            );
        }
        if( isset( $models[$n+1] ) ) {
            $cfg['next'] = 'olHelpGuider_'.$models[$n+1]->id;
            $buttons[] = array(
                'name'   => Yii::t('onlinehelp','Next'),
                'onclick'=> "js:function(){guiders.next();}"
            );
        }
        if( $n==count($models)-1)
            $cfg['description'] .= $checkbox;
        $buttons[] = array(
            'name'   => Yii::t('onlinehelp','Exit'),
            'onclick'=> "js:function(){guiders.hideAll();}"
        );
        $cfg['buttons'] = $buttons;
        $this->widget('ext.eguiders.EGuider', $cfg);
    }
} // }}} 
?>
<div style="text-align:right;padding:7px 15px 7px 0;">
<a href="javascript:void(0)" title="<?php echo CHtml::encode('Show Online Help'); ?>" onclick="$('.alert-online-help').toggle(400)">?</a>
</div>
<div class="alert alert-online-help alert-block alert-success" style="display:none">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4><?php echo Yii::t('onlinehelp','Online Help').CHtml::encode($title); ?>&nbsp;&nbsp;&nbsp;
    <?php if($userIsEditor==true) : ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>$label,
        'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'icon'=>'pencil white',
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'url'=>$url,
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>Yii::t('onlinehelp','Manage'),
        'icon'=>'th-list',
        'size'=>'small', // null, 'large', 'small' or 'mini'
        'url'=>array('/onlinehelp'),
    )); ?>
    <?php endif; ?>
    </h4>
    <?php echo $content; ?>
    <?php echo $checkbox; ?>
</div>


<?php Yii::app()->clientScript->registerScript('olhelp','shortcut.add("'.Yii::t('onlinehelp','OLHelpKey').'",function() {$(".alert-online-help").toggle(400);},{disable_in_input:true});');; ?>
<script type="text/javascript">
<!--
function togglePageHelp(checkbox, requestId)
{
    var isChecked = jQuery(checkbox).attr('checked');
    // DEBUG alert( "Request: " + requestId + ", " + isChecked );
    $.ajax({
        url: "<?php echo Yii::app()->controller->createUrl('/onlinehelp/editor/toggleUserOnlineHelpPage'); ?>?requestId=" + requestId.replace(/\//g, ".") + "&isChecked=" + isChecked,
        context: document.body,
        dataType: 'json'
    }).done(function(data) {
        if( data.status=='SUCCESS' ) alert( '<?php echo CHtml::encode(Yii::t('onlinehelp', "The settings have been saved. You can toggle this setting by pressing H.")); ?>' );
    });
}
// -->
</script>
