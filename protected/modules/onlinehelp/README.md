yii-onlinehelp
==============

**Onlinehelp** is a module for the [Yii PHP framework](http://www.yiiframework.com) that provides a web user interface to generate and maintain an online help system.

_Onlinehelp_ was developed to provide a modern and responsive user interface for managing online help text in Yii projects.
To achieve its goals it was built using the popular [Twitter Bootstrap extension](http://www.yiiframework.com/extension/bootstrap).

_Onlinehelp_ is written according to Yii's conventions.


### Demo

You can try out the live demo [here](http://www.diggin-data.de/demos/yii-onlinehelp/).


### Requirements

* [Twitter Bootstrap extension for Yii](http://www.yiiframework.com/extension/bootstrap) version 2.0.0 or above


## Usage


### Setup

Download the latest release from [Yii extensions](http://www.yiiframework.com/extension/onlinehelp).

Unzip the module under ***protected/modules/onlinehelp*** and add the following to your application config:

~~~
// protected/config/main.php:
return array(
  ...
  'modules' => array(
    'onlinehelp',
  ),
  ...
);
~~~

Please note that the module requires you to use a database, if you wish to use **Onlinehelp** you need it's schema (it can be found in the module directory under ***/data/...schema.sql***).


### Configuration

Configure the module to suit your needs. Here's a list of the available configurations (with default values).

~~~
'modules'=>array(
  ...
  'onlinehelp' => array(
    'users' => array('admin'), // a list of users who has access to the module.
    'userIdColumn' => 'id', // the name of the user id column.
    'layout' => 'application.views.layouts.main', // the layout used by the module.
  ),
  ...
~~~


### Showing the Online Help Widget

Insert the following code in your ***views/layouts/column...php*** file, just after ***<?php $this->beginContent('//layouts/main'); ?>***:

~~~
<?php
// Get path info
$pathInfo = array();
if(!is_null(Yii::app()->controller->module))
    $pathInfo[] = Yii::app()->controller->module->id;
$pathInfo[] = Yii::app()->controller->id;
$pathInfo[] = Yii::app()->controller->action->id; 
$this->widget('application.modules.onlinehelp.components.OnlineHelpWidget',array('pathInfo'=>join('/',$pathInfo)));
?>
~~~

This will display a **?** link before the main content, aligned to the right. If clicked, the online help area for the current request will be toggled.  
Alternatively press the **H key** to toggle the online help area (the H key might not work on some browsers).


### Adding Help Text

If there is no online help yet for a single page, a link to create the new help text is shown inside the online help area.

Create and save the help text in the form. The module returns to the calling page, and by clicking on **?**, the new help text is shown.

You may also manage help texts via the _...?r=onlinehelp/editor_ URL.

### Guiders

By default, _Page Help_ text is shown as a popup dialog, when the page is opened.

You may also want to add **guiders** to some specific elements on your page, not just a general help text for the whole page.

To do so, create entries in the onlinehelp table, where **Page Help** is _not_ checked and an **Element ID** is specified.  
The element-specific help will be shown in consecutive popups after the page-help popup.  
Use the **Sequence No.** field to define the sequence of popups.  
For **Element ID**, you can use any valid [jQuery selectors](http://api.jquery.com/category/selectors/).

### Internationalization

Do you wish to use a translation for Onlinehelp? If so, please copy _onlinehelp/messages/*_ folders to _protected/messages_.  
Translations should be placed in the messages folder under a folder named according to its locale (e.g. ***en_us***).

## Thanks To

* [Guiders-JS](https://github.com/jeff-optimizely/Guiders-JS)
* Raoul for [equiders](http://www.yiiframework.com/extension/eguiders/)
* Christoffer Niska for [yii-bootstrap](http://www.cniska.net/yii-bootstrap/)
* [Yii Framework](http://www.yiiframework.com)
