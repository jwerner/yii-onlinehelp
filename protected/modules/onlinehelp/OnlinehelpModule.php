<?php

class OnlinehelpModule extends CWebModule
{
    // {{{ Members
	/**
	 * @var array a list of names for users that has access to this module.
	 */
	public $users = array('admin');
	/**
	 * @var string name of the user id column.
	 * Change this if the id column in your user table is different than the default value.
	 */
	public $userIdColumn = 'id';
	/**
	 * @var string the application layout.
	 * Change this if you wish to use a different layout with the module.
	 */
    public $layout = '//layouts/main';
	/**
	 * @var string string the id of the default controller for this module.
	 */
	public $defaultController = 'editor';
    
    private $_assetsUrl;
    // }}} 
    // {{{ init
    public function init()
    {
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
        $this->setImport(array(
            'onlinehelp.models.*',
            'onlinehelp.components.*',
			'onlinehelp.models.*',
			'onlinehelp.components.*',
		));
        $this->registerCss();
	} // }}} 
    // {{{ beforeControllerAction
	/**
	 * The pre-filter for controller actions.
	 * @param CController $controller the controller.
	 * @param CAction $action the action.
	 * @return boolean whether the action should be executed.
	 * @throws CHttpException if user is denied access.
	 */
	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action))
        {
            if(is_array($this->users)) {
    			if (!in_array(Yii::app()->user->getName(), $this->users))
	    			throw new CHttpException(401, Yii::t('onlinehelp', 'Access denied.'));
            }

			return true;
		}
		else
			return false;
    } // }}} 
    // {{{ registerCss
    /**
	 * Registers the module CSS.
	 */
	public function registerCss()
	{
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/onlinehelp.css');
    } // }}} 
    // {{{ getAssetsUrl
	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL.
	 */
	protected function getAssetsUrl()
	{
        if (isset($this->_assetsUrl))
        {
            Yii::log('assetsUrl: '.$this->_assetsUrl);
			return $this->_assetsUrl;
        }
        else
		{
			$assetsPath = Yii::getPathOfAlias('onlinehelp.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath); //, false, -1, $this->forceCopyAssets);
            Yii::log('assetsUrl: '.$assetsUrl);

			return $this->_assetsUrl = $assetsUrl;
		}
    } // }}} 
}
