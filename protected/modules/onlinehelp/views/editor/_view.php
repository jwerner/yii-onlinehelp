<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tsCreated')); ?>:</b>
	<?php echo CHtml::encode($data->tsCreated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userIdCreated')); ?>:</b>
	<?php echo CHtml::encode($data->userIdCreated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tsUpdated')); ?>:</b>
	<?php echo CHtml::encode($data->tsUpdated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('userIdUpdated')); ?>:</b>
	<?php echo CHtml::encode($data->userIdUpdated); ?>
	<br />


</div>