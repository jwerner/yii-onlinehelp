<?php
$this->breadcrumbs=array(
    // Module Yii::t('onlinehelp','MODULE_NAME')=>array('/MODULE_ID'),
    Yii::t('onlinehelp','Online Help Pages'),
);

$this->menu=array(
    array('label'=>Yii::t('onlinehelp','Create New Online Help Page'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('onlinehelp','Manage Online Help Pages'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('onlinehelp','Online Help Pages')); ?></h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
    'template'=>'{sorter}{pager}{summary}{items}{sorter}{pager}',
    'sortableAttributes'=>array(
    'title',
    'content',
    'tsCreated',
    // TODO: Uncomment the following attributes to show them as sort links in list view
// 'userIdCreated',
    // 'tsUpdated',
    // 'userIdUpdated',
    ),
)); ?>
