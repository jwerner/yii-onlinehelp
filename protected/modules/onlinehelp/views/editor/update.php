<?php
$this->breadcrumbs=array(
    // Module Yii::t('onlinehelp','MODULE_NAME')=>array('/MODULE_ID'),
	Yii::t('onlinehelp','Online Help Pages')=>array('admin'),
	$model->title=>array('view','id'=>$model->id),
	Yii::t('onlinehelp','Update'),
);

$this->menu=array(
    array('label'=>Yii::t('onlinehelp','Create New Online Help Page'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>Yii::t('onlinehelp','View Online Help Page'),'url'=>array('view','id'=>$model->id), 'icon'=>'eye-open'),
	array('label'=>Yii::t('onlinehelp','List Online Help Pages'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('onlinehelp','Manage Online Help Pages'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('onlinehelp','Update Online Help Page {recordName}',array('{recordName}'=>$model->recordName))); ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
