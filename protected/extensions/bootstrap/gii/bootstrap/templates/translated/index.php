<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label',
);\n";
?>

$this->menu=array(
	array('label'=>Yii::t('site','Create New <?php echo $this->modelClass; ?>'),'url'=>array('create'), 'icon'=>'star'),
	array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo $label; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
    'itemView'=>'_view',
    'template'=>'{sorter}{pager}{summary}{items}{sorter}{pager}',
    'sortableAttributes'=>array(
<?php
// Add the first three attributes as sortable
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if($column->isPrimaryKey)
        continue;
    echo "    ";
    $count++;
    if($count==4)
        echo "// TODO: Uncomment the following attributes to show them as sort links in list view\n";
	if($count>=4)
		echo "// ";
	echo "'{$column->name}',\n";
}
?>
    ),
)); ?>
