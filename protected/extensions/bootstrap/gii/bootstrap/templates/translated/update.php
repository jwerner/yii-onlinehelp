<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('site','$label')=>array('index'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	Yii::t('site','Update'),
);\n";
?>

$this->menu=array(
	array('label'=>'Create New <?php echo $this->modelClass; ?>','url'=>array('create'), 'icon'=>'star'),
	array('label'=>'View <?php echo $this->modelClass; ?>','url'=>array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>), 'icon'=>'eye-open'),
	array('label'=>'List <?php echo $label; ?>','url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>'Manage <?php echo $label; ?>','url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo "<?php echo CHtml::encode(Yii::t('site','Update {$this->class2name($this->modelClass)} #{primaryKey}',array('{primaryKey}'=>\$model->{$this->tableSchema->primaryKey}))); ?>"; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>
