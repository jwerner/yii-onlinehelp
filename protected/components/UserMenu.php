<?php

Yii::import('zii.widgets.CPortlet');


class UserMenu extends CWidget
{
    public $title;

    //public $decorationCssClass='portlet well';
	public function init()
	{
		$this->title=CHtml::encode(Yii::t('site','User:').' '.Yii::app()->user->name);
		parent::init();
	}

	public function run()
	{
		$this->render('userMenu',array('title'=>$this->title));
	}
}
