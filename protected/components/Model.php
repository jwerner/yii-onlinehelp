<?php 
/**
 * Model class file
 */

class Model extends CActiveRecord
{
    public function save($runValidation=true,$attributes=null)
    {
        if(in_array($this->tableName(), array('{{post}}', '{{onlinehelp}}'))) {
            Yii::app()->user->setFlash('warning', '<strong>'.Yii::t('site','Info').'</strong><br />'.Yii::t('site','Not allowed to save items in table {tableName} in this demo version.', array('{tableName}'=>str_replace(array('{{','}}'),array('',''),$this->tableName()) ) ) );
            return true;
        }
        return  parent::save($runValidation,$attributes);
    }
}
