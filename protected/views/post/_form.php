<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'post-form',
    'type'=>'horizontal',
    'focus'=>array($model,$model->isNewRecord ? 'title' : 'content'),
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo CHtml::errorSummary($model); ?>

    <?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>128)); ?>

    <?php echo $form->textAreaRow($model,'content',array('rows'=>10, 'class'=>'span5', 'hint'=>'You may use <a target="_blank" href="http://daringfireball.net/projects/markdown/syntax">Markdown syntax</a>.')); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model,'tags',array('class'=>'control-label required')); ?>
        <div class="controls">
        <?php $this->widget('CAutoComplete', array(
            'model'=>$model,
            'attribute'=>'tags',
            'url'=>array('suggestTags'),
            'multiple'=>true,
            'htmlOptions'=>array('class'=>'span5'),
        )); ?>
        <p class="hint">Please separate different tags with commas.</p>
        <?php echo $form->error($model,'tags'); ?>
        </div>
    </div>

    <?php echo $form->dropDownListRow($model,'status',Lookup::items('PostStatus')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok', 'label'=>$model->isNewRecord ? Yii::t('blog','Create') : Yii::t('blog','Save'))); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'icon'=>'repeat', 'label'=>Yii::t('site','Reset'))); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
