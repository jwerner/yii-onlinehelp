<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="span9">
<?php
// Get path info
$pathInfo = array();
if(!is_null(Yii::app()->controller->module))
    $pathInfo[] = Yii::app()->controller->module->id;
$pathInfo[] = Yii::app()->controller->id;
$pathInfo[] = Yii::app()->controller->action->id; 
$this->widget('application.modules.onlinehelp.components.OnlineHelpWidget',array('pathInfo'=>join('/',$pathInfo)));
?>
        <div id="content">
            <?php $this->widget('bootstrap.widgets.TbAlert', array(
                    'block'=>true, // display a larger alert block?
                    'fade'=>true, // use transitions?
                    'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
                    'alerts'=>array( // configurations per alert type
                        'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
                        'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
                        'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
                    ),
                )); ?>
            <?php echo $content; ?>
            <?php // echo '<pre>'; CVarDumper::dump($_SESSION); echo '</pre>'; ?>
        </div><!-- content -->
    </div>
    <div class="span3">
        <div id="sidebar">
            <?php if(!Yii::app()->user->isGuest) $this->widget('UserMenu'); ?>
            <?php if(!Yii::app()->user->isGuest) $this->widget('OperationsMenu'); ?>

            <?php $this->widget('TagCloud', array(
                'maxTags'=>Yii::app()->params['tagCloudCount'],
            )); ?>

            <?php $this->widget('RecentComments', array(
                'maxComments'=>Yii::app()->params['recentCommentCount'],
            )); ?>
        </div><!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>
