<?php
// Set the path of Bootstrap to be the root of the project.
Yii::setPathOfAlias('bootstrap', realpath(dirname(__FILE__).'/../extensions/bootstrap/'));

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$config = array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Yii Blog Demo with Online Help',
    'sourceLanguage' => 'en_us',

    // preloading 'log' component
    'preload'=>array('log','bootstrap','lc'),

    // autoloading model and component classes
    'import'=>array(
        'application.models.*',
        'application.components.*',
    ),

    'defaultController'=>'site',
    'modules'=>array( // {{{ 
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'gii',
            'generatorPaths'=>array(
                'bootstrap.gii',
            ),
        ),
        'onlinehelp' => array(
            'users' => array('admin'), // a list of users who has access to the module.
            'userIdColumn' => 'id', // the name of the user id column.
            'layout' => '//layouts/main', // the layout used by the module.
        ),
    ), // }}} 
    // application components
    'components'=>array(
        'bootstrap'=>array( // {{{ 
            'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
            'responsiveCss'=>true,
        ), // }}} 
        /*
        'db'=>array(
            'connectionString' => 'sqlite:protected/data/blog.db',
            'tablePrefix' => 'tbl_',
        ),
         */
        // uncomment the following to use a MySQL database
        /*
        'db'=>array(
            'connectionString' => 'mysql:host=localhost;dbname=yii_blog',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
        ),
        */
        'errorHandler'=>array( // {{{ 
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ), // }}} 
        'lc'=>array( // {{{ 
            'class' => 'application.components.LocaleManager',
        ), // }}} 
        'log'=>array( // {{{ 
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ), // }}} 
        'session'=>array( // {{{ 
            'sessionName'=>'YII-BLOG-BS-OLHELP'
        ), // }}} 
        'urlManager'=>array( // {{{ 
            'urlFormat'=>'path',
            'rules'=>array(
                'post/<id:\d+>/<title:.*?>'=>'post/view',
                'posts/<tag:.*?>'=>'post/index',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ), // }}} 
        'user'=>array( // {{{ 
            'class'=>'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ), // }}} 
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>require(dirname(__FILE__).'/params.php'),
);
return file_exists(dirname(__FILE__).'/main-local.php')
    ? CMap::mergeArray($config, require('main-local.php'))
    : $config;
